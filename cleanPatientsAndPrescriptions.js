const { MongoClient } = require('mongodb');

const client = new MongoClient('mongodb://127.0.0.1:27017', {
  useUnifiedTopology: true,
});

const run = async () => {
  /**
   * Init mongo client
   */
  await client.connect();
  await client.db('admin').command({ ping: 1 });
  const db = client.db('admin');

  await db.collection('prescriptionAssistancePrescriptions').deleteMany({});
  await db.collection('prescriptionAssistancePatients').deleteMany({});
};

run().catch(console.error);
