# How to run

```bash
# Port forward to test db
kubectl port-forward service/synapse-database 27017

# run script
node ./cleanPatientsAndPrescriptions.js
```